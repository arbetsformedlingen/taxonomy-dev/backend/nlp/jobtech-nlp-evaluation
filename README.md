# jobtech-nlp-evaluation

This repository contains code for evaluating the effectiveness of NLP solutions developed at Jobtech.

## Usage

To do.

## Testing

Run the unit tests using the [clj](https://clojure.org/guides/deps_and_cli) tools:
```
clj -X:test
```

## Licence

Copyright © 2021 JobTech

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
