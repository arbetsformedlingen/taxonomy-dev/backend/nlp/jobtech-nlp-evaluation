(ns jobtech-nlp-evaluation.annotation
  (:require [clojure.spec.alpha :as spec]
            [jobtech-nlp-evaluation.confusion-matrix :as confusion-matrix]))

(spec/def :annotation/concept-id string?)
(spec/def :annotation/preferred-label string?)
(spec/def :annotation/type string?)
(spec/def :annotation/start-position number?)
(spec/def :annotation/end-position number?)
(spec/def :annotation/matched-string? string?)
(spec/def :annotation/type string?)
(spec/def :annotation/id number?)
(spec/def :annotation/annotation (spec/keys :req-un [:annotation/concept-id
                                                     :annotation/preferred-label
                                                     :annotation/type
                                                     :annotation/start-position
                                                     :annotation/end-position
                                                     :annotation/matched-string
                                                     :annotation/annotation-id]))
(spec/def :annotation/annotations (spec/coll-of :annotation/annotation))
(spec/def :annotation/annotated-document (spec/keys :req-un [:annotation/annotations]))


;;; Features

(defn distinct-annotation-feature
  "Select only enough information from an annotation to distinctly identify it. This tests for each annotation whether it is present at that position in the document."
  
  ([] :annotation-exists?)
  ([annotation]
   (select-keys annotation [:concept-id :start-position])))

(defn distinct-concept-feature
  "Select only the concept of an annotation and ignore its position within the document. This means that we test whether or not a concept is present in the document."

  ([] :concept-exists?)
  ([annotation]
   (select-keys annotation [:concept-id])))

;;; Computing the confusion matrix

(def annotated-document? (partial spec/valid? :annotation/annotated-document))

(defn document-annotation-features
  "Map all annotations into a set using a function 
that only retains the distinct features of every annotation"
  
  [annotation-feature-fn document]
  (into #{} (map annotation-feature-fn) (:annotations document)))

(defn document-confusion-matrix
  "Compute a confusion matrix for a document given 
the actual and predicted concept annotations"
  [feature-fn actual-annotations predicted-annotations]
  {:pre [(fn? feature-fn)
         (annotated-document? actual-annotations)
         (annotated-document? predicted-annotations)]}
  (let [actual-features (document-annotation-features feature-fn actual-annotations)
        predicted-features (document-annotation-features feature-fn predicted-annotations)
        all-features (into actual-features predicted-features)]
    (transduce (map #(vector (contains? actual-features %)
                             (contains? predicted-features %)))
               confusion-matrix/accumulate-pair
               (confusion-matrix/empty-confusion-matrix (feature-fn))
               all-features)))

;; This function compute the confusion matrix for distinct annotations
(def annotation-confusion-matrix (partial document-confusion-matrix distinct-annotation-feature))
