(ns jobtech-nlp-evaluation.confusion-matrix)

;;; Private helpers
(defn- divide-or-nil [a b]
  (if (not (zero? b))
    (/ a b)))

(defn- inc-pair [dst pair i]
  (update dst pair #(+ i (or % 0))))

;;;------- Construct and test if something is a confusion matrix -------

(defn empty-confusion-matrix
  "Construct an empty confusion matrix with a tag that represents what this matrix is about"
  [tag]
  {:confusion-matrix-tag tag})

(defn confusion-matrix?
  "Test if something is a confusion matrix"
  [x]
  (and (map? x)
       (contains? x :confusion-matrix-tag)))

;;;------- Pairs -------

(defn actual-predicted-pair?
  "Test if something is a pair"
  [x]
  (and (sequential? x)
       (= 2 (count x))))

(defn actual-predicted-pair
  "Construct a pair"
  [actual-class predicted-class]
  [actual-class predicted-class])


;;;------- Accessing values of a matrix -------

(defn data
  "Access only the data part of a matrix, omitting the tag of what the data means"
  [confusion-matrix]
  {:pre [(confusion-matrix? confusion-matrix)]}
  (dissoc confusion-matrix :confusion-matrix-tag))

(defn classes
  "Get all the classes that are part of the matrix"
  [confusion-matrix]
  {:pre [(confusion-matrix? confusion-matrix)]}
  (into #{} cat (-> confusion-matrix data keys)))

(defn tag
  "Get the tag that encodes what the confusion matrix means"
  [confusion-matrix]
  {:pre [(confusion-matrix? confusion-matrix)]}
  (:confusion-matrix-tag confusion-matrix))

(defn get-element
  "Get an element of the confusion matrix or nil if there is no element"
  [matrix pair]
  (get matrix pair))

(defn get-count
  "Get the element of the confusion matrix or 0 if there is no element"
  [matrix pair]
  (or (get-element matrix pair) 0))

(defn count-filtered
  "Compute the sum of matrix elements that satisfy a predicate"
  [matrix pair-predicate]
  (transduce (comp (filter (comp pair-predicate first))
                   (map second))
             +
             0
             (data matrix)))

(defn compatible?
  "Test whether the matrices are of the same type"
  [matrices]
  (and (every? confusion-matrix? matrices)
       (let [x (tag (first matrices))]
         (every? #(= x (tag %)) matrices))))

;;;------- Operations -------

(defn accumulate-pair
  "Accumulate a single actual-predicted pair to the matrix"
  ([confusion-matrix pair]
   (inc-pair confusion-matrix pair 1))
  ([confusion-matrix] confusion-matrix))

(defn add-matrices
  "Add several confusion matrices"
  [& matrices]
  {:pre [(compatible? matrices)]
   :post [(confusion-matrix? %)]}
  (transduce (mapcat data)
             (completing (fn [dst [pair i]] (inc-pair dst pair i)))
             (first matrices)
             (rest matrices)))


;;;------- Metrics -------

;; Counts
(defn total-count [m]
  (count-filtered m (constantly true)))

(defn true-positive [m]
  (get-count m [true true]))

(defn false-positive [m]
  (get-count m [false true]))

(defn true-negative [m]
  (get-count m [false false]))

(defn false-negative [m]
  (get-count m [true false]))

(defn count-actual [m cl]
  (count-filtered m (fn [[actual-class _]] (= cl actual-class))))

(defn condition-positive [m]
  (count-actual m true))

(defn condition-negative [m]
  (count-actual m false))

;; Scores derived from counts
(defn precision [m]
  (let [tp (true-positive m)
        fp (false-positive m)]
    (divide-or-nil tp (+ tp fp))))

(defn recall [m]
  (divide-or-nil (true-positive m)
                 (condition-positive m)))

(defn f1 [m]
  (let [tp2 (* 2 (true-positive m))]
    (divide-or-nil tp2
                   (+ tp2
                      (false-positive m)
                      (false-negative m)))))
