(ns jobtech-nlp-evaluation.annotation-test
  (:require [clojure.test :refer :all]
            [jobtech-nlp-evaluation.annotation :refer :all]
            [jobtech-nlp-evaluation.confusion-matrix :as cm]
            [clojure.edn :as edn]
            [clojure.spec.alpha :as spec]
            [clojure.java.io :as io]))

(defn load-sample-document []
  (-> "annotated-example-education.edn"
      io/resource
      slurp
      edn/read-string))

(deftest annotation-test
  (let [doc (load-sample-document)
        doc-a (update doc :annotations #(drop 2 %))
        doc-b (update doc :annotations #(drop 3 (reverse %)))]
    (is (spec/valid? :annotation/annotated-document doc))
    (let [self-mat (annotation-confusion-matrix doc doc)
          n (cm/total-count self-mat)

          ab-mat (annotation-confusion-matrix doc-a doc-b)
          ba-mat (annotation-confusion-matrix doc-b doc-a)]

      ;; Check that we build the confusion matrix for some nontrivial amount of annotations
      (is (< 8 n))
      
      (is (= n (cm/true-positive self-mat)))
      (is (= n (cm/total-count ab-mat)))
      (is (= n (cm/total-count ba-mat)))
      (is (= 2 (cm/false-positive ab-mat)))
      (is (= 3 (cm/false-positive ba-mat)))
      (is (= 3 (cm/false-negative ab-mat)))
      (is (= 2 (cm/false-negative ba-mat)))
      (is (= (- n 2 3) (cm/true-positive ab-mat)))
      (is (= (- n 2 3) (cm/true-positive ba-mat))))))
