(ns jobtech-nlp-evaluation.confusion-matrix-test
  (:require [jobtech-nlp-evaluation.confusion-matrix :refer :all]
            [clojure.test :refer :all]))

(deftest confusion-matrix-test
  (is (not (confusion-matrix? {})))
  (is (not (confusion-matrix? [:mjao])))
  (is (confusion-matrix? (empty-confusion-matrix :annotation-exists?)))
  (is (= {:confusion-matrix-tag :annotation-exists?
          [false true] 1
          [true true] 2}
         (reduce accumulate-pair
                 (empty-confusion-matrix :annotation-exists?)
                 [[true true]
                  [false true]
                  [true true]])))
  (is (= {:confusion-matrix-tag :mjao
          [false true] 2
          [true true] 3}
         (add-matrices {:confusion-matrix-tag :mjao
                        [false true] 1}
                       {:confusion-matrix-tag :mjao
                        [false true] 1
                        [true true] 3})))
  (is (= {[false true] 2
          [true true] 3}
         (data {:confusion-matrix-tag :mjao
                [false true] 2
                [true true] 3})))
  (is (= #{false true}
         (classes {:confusion-matrix-tag :mjao
                   [false true] 9})))
  (is (= :mjao (tag (empty-confusion-matrix :mjao)))))

(def mat (reduce accumulate-pair
                 (empty-confusion-matrix :exists?)
                 [[false true]
                  [false true]
                  [false true]
                  [true true]
                  [true true]
                  [true false]]))

(deftest metrics-test
  (is (= 6 (total-count mat)))
  (is (= 2 (true-positive mat)))
  (is (= 3 (false-positive mat)))
  (is (= 1 (false-negative mat)))
  (is (= 3 (condition-positive mat)))
  (is (= 3 (condition-negative mat)))
  (is (= 2/5 (precision mat)))
  (is (= 2/3 (recall mat)))
  (is (= 1/2 (f1 mat)))
  (doseq [metric [f1 recall precision]]
    (is (nil? (metric (empty-confusion-matrix :mjao)))))
  (doseq [metric [total-count
                  true-positive
                  false-positive
                  false-negative
                  true-negative
                  condition-positive
                  condition-negative]]
    (is (zero? (metric (empty-confusion-matrix :mjao))))))
